package application;

import application.domain.User;
import application.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

/**
 * Created by john on 17/05/2017.
 */
@Component
@Service
public class AuthenticateService {

    @Autowired
    private UserRepository userRepository;

    private TokenStore tokenStore = CustomTokenStore.getInstance();

    public AuthenticateService()
    {
        this.tokenStore = CustomTokenStore.getInstance();
    }

    public User getUserByAccessToken(String accessToken)
    {
        // Get an OAuth2 access token (class) from the OAuth2 access token string.
        OAuth2Authentication oAuth2Authentication = this.StringToOauth2AccessToken(accessToken);

        // Extract the primary key from the access token class
        String email = oAuth2Authentication.getName();

        return this.getUserByEmail(email);
    }

    private OAuth2Authentication StringToOauth2AccessToken(String oauth2AccessToken)
    {
        String cleanAccessToken = oauth2AccessToken.replaceAll("Bearer ", "");

        return tokenStore.readAuthentication(cleanAccessToken);
    }

    private User getUserByEmail(String email)
    {
        return this.userRepository.findByEmail(email);
    }

}
