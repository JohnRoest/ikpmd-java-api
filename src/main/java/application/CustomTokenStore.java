package application;

import org.springframework.security.oauth2.provider.token.TokenStore;
import org.springframework.security.oauth2.provider.token.store.InMemoryTokenStore;

/**
 * Created by john on 12/05/2017.
 */
public class CustomTokenStore {

    private static TokenStore tokenStore;

    private CustomTokenStore() {}

    public static TokenStore getInstance()
    {
        if(tokenStore == null)
        {
            tokenStore = new InMemoryTokenStore();
        }
        return tokenStore;
    }
}
