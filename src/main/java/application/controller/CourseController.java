package application.controller;

import application.domain.Course;
import application.domain.StudyGoal;
import application.domain.User;
import application.repository.CourseRepository;
import application.repository.StudyGoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class CourseController extends BaseController {

    @Autowired
    private CourseRepository courseRepository;

    @Autowired
    private StudyGoalRepository studyGoalRepository;

    @RequestMapping(path = "/course",
                    consumes = APPLICATION_JSON,
                    produces = APPLICATION_JSON,
                    method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody Course course,
                                 @RequestHeader("Authorization") String accessToken)
    {
        User user = super.userByAccessToken(accessToken);

        course.setUser(user);

        Course savedCourse = this.courseRepository.save(course);
        Iterable<StudyGoal> savedStudyGoals = this.studyGoalRepository.save(course.getStudyGoals());

        Course updatedCourse = this.updateCourseStudyGoals(savedCourse, savedStudyGoals);
        return new ResponseEntity<>(updatedCourse, HttpStatus.OK);
    }

    @RequestMapping(path = "/course",
                    produces = APPLICATION_JSON,
                    method = RequestMethod.GET)
    public ResponseEntity all(@RequestHeader("Authorization") String accessToken)
    {
        User user = super.userByAccessToken(accessToken);
        Iterable<Course> courseIterable = this.courseRepository.findAllByUser(user);
        List<Course> courses = new ArrayList<>();

        courseIterable.iterator().forEachRemaining(courses::add);

        return new ResponseEntity<>(courses, HttpStatus.OK);
    }

    @RequestMapping(path = "/course/{id}",
                    produces = APPLICATION_JSON,
                    method = RequestMethod.GET)
    public ResponseEntity read(@RequestHeader("Authorization") String accessToken,
                               @PathVariable Integer id)
    {
        User user = super.userByAccessToken(accessToken);

        Course course = this.courseRepository.findOne(id);
        if (course.getUser().getEmail().equals(user.getEmail()))
        {
            return new ResponseEntity<>(course, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(path = "/course/{id}",
                    consumes = APPLICATION_JSON,
                    produces = APPLICATION_JSON,
                    method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody Course newCourse,
                                 @RequestHeader("Authorization") String accessToken,
                                 @PathVariable Integer id)
    {
        User user = super.userByAccessToken(accessToken);

        Course course = this.courseRepository.findOne(id);
        if (course.getUser().getEmail().equals(user.getEmail()))
        {
            Course updatedCourse = this.courseRepository.save(newCourse);
            return new ResponseEntity<>(updatedCourse, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(path = "/course/{id}",
            produces = APPLICATION_JSON,
            method = RequestMethod.DELETE)
    public ResponseEntity delete(@RequestHeader("Authorization") String accessToken,
                                 @PathVariable Integer id)
    {
        User user = super.userByAccessToken(accessToken);

        Course course = this.courseRepository.findOne(id);
        if (course.getUser().getEmail().equals(user.getEmail()))
        {
            this.studyGoalRepository.deleteAllByCourse(course);
            this.courseRepository.delete(id);

            return new ResponseEntity<>(true, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(false, HttpStatus.FORBIDDEN);
        }
    }


    private Course updateCourseStudyGoals(Course course,
                                          Iterable<StudyGoal> studyGoalIterable)
    {
        ArrayList<StudyGoal> studyGoals = new ArrayList<>();
        studyGoalIterable.iterator().forEachRemaining(studyGoals::add);

        for(StudyGoal studyGoal : studyGoals)
        {
            studyGoal.setCourse(course);
        }

        course.setStudyGoals(studyGoals);

        this.studyGoalRepository.save(studyGoals);

        return this.courseRepository.save(course);
    }

}
