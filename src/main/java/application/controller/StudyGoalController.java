package application.controller;

import application.domain.StudyGoal;
import application.domain.User;
import application.repository.StudyGoalRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class StudyGoalController extends BaseController {

    @Autowired
    private StudyGoalRepository studyGoalRepository;

    @RequestMapping(path = "/studygoal",
                    consumes = APPLICATION_JSON,
                    produces = APPLICATION_JSON,
                    method = RequestMethod.POST)
    public ResponseEntity create(@RequestBody StudyGoal studyGoal,
                                 @RequestHeader("Authorization") String accessToken)
    {
        User user = super.userByAccessToken(accessToken);

        studyGoal.setUser(user);

        StudyGoal savedStudyGoal = this.studyGoalRepository.save(studyGoal);
        return new ResponseEntity<>(savedStudyGoal, HttpStatus.OK);
    }

    @RequestMapping(path = "/studygoal/{id}",
                    produces = APPLICATION_JSON,
                    method = RequestMethod.GET)
    public ResponseEntity read(@RequestHeader("Authorization") String accessToken,
                               @PathVariable Integer id)
    {
        User user = super.userByAccessToken(accessToken);

        StudyGoal studyGoal = this.studyGoalRepository.findOne(id);
        if (studyGoal.getUser().getEmail().equals(user.getEmail()))
        {
            return new ResponseEntity<>(studyGoal, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(path = "/studygoal/{id}",
            consumes = APPLICATION_JSON,
            produces = APPLICATION_JSON,
            method = RequestMethod.PUT)
    public ResponseEntity update(@RequestBody StudyGoal newStudyGoal,
                                 @RequestHeader("Authorization") String accessToken,
                                 @PathVariable Integer id)
    {
        User user = super.userByAccessToken(accessToken);

        if (newStudyGoal.getUser().getEmail().equals(user.getEmail()))
        {
            StudyGoal updatedStudyGoal = this.studyGoalRepository.save(newStudyGoal);
            return new ResponseEntity<>(updatedStudyGoal, HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

    @RequestMapping(path = "/studygoal/{id}",
            produces = APPLICATION_JSON,
            method = RequestMethod.DELETE)
    public ResponseEntity delete(@RequestHeader("Authorization") String accessToken,
                                 @PathVariable Integer id)
    {
        User user = super.userByAccessToken(accessToken);

        StudyGoal studyGoal = this.studyGoalRepository.findOne(id);
        if (studyGoal.getUser().getEmail().equals(user.getEmail()))
        {
            this.studyGoalRepository.delete(id);
            return new ResponseEntity<>(HttpStatus.OK);
        }
        else
        {
            return new ResponseEntity<>(HttpStatus.FORBIDDEN);
        }
    }

}
