package application.controller;

import application.AuthenticateService;
import application.domain.User;
import org.springframework.beans.factory.annotation.Autowired;

public abstract class BaseController {

    @Autowired
    private AuthenticateService authenticateService;

    public final String APPLICATION_JSON = "application/json";

    public User userByAccessToken(String accessToken)
    {
        return this.authenticateService.getUserByAccessToken(accessToken);
    }
}
