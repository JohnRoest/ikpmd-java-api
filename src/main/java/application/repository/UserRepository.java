package application.repository;

import application.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository()
@Transactional
public interface UserRepository extends CrudRepository<User, String> {

    User findByEmail(String email);

}
