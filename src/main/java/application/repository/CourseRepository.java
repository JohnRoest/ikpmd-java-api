package application.repository;

import application.domain.Course;
import application.domain.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository()
@Transactional
public interface CourseRepository extends CrudRepository<Course, Integer> {

     Iterable<Course> findAllByUser(User user);
}
