package application.repository;

import application.domain.Course;
import application.domain.StudyGoal;
import javax.transaction.Transactional;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository()
@Transactional
public interface StudyGoalRepository extends CrudRepository<StudyGoal, Integer> {

    void deleteAllByCourse(Course course);

}
