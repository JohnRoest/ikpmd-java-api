package application;

import org.springframework.context.annotation.Configuration;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;
import org.springframework.security.oauth2.config.annotation.web.configuration.ResourceServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configurers.ResourceServerSecurityConfigurer;

@Configuration
@EnableResourceServer
public class ResourceServerConfiguration extends ResourceServerConfigurerAdapter {

  private final String RESOURCE_ID = "LINEBANK_WEB_API";

  @Override
  public void configure(ResourceServerSecurityConfigurer resources)
  {
    resources.resourceId(RESOURCE_ID).stateless(true);
  }
}