package application.domain;

import com.fasterxml.jackson.annotation.JsonManagedReference;
import org.codehaus.jackson.annotate.JsonBackReference;

import javax.annotation.Nullable;
import javax.persistence.*;
import java.util.List;

@Entity
public class Course {

    @Id
    @GeneratedValue
    @Column(name="id")
    @Nullable
    private Integer id;

    @OneToOne
    private User user;

    @Column(name="name")
    private String name;

    @OneToMany(mappedBy="course")
    @JsonManagedReference
    private List<StudyGoal> studyGoals;

    public Course(Integer id,
                  User user,
                  String name,
                  List<StudyGoal> studyGoals)
    {
        this.id = id;
        this.user = user;
        this.name = name;
        this.studyGoals = studyGoals;
    }

    public Course(User user,
                  String name,
                  List<StudyGoal> studyGoals)
    {
        this.user = user;
        this.name = name;
        this.studyGoals = studyGoals;
    }

    public Course() {}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public List<StudyGoal> getStudyGoals() {
        return studyGoals;
    }

    public void setStudyGoals(List<StudyGoal> studyGoals) {
        this.studyGoals = studyGoals;
    }

    @Override
    public String toString() {
        return "Course{" +
                "\nid=" + id +
                ",\n user=" + user +
                ",\n name='" + name + '\'' +
                ",\n studyGoals=" + studyGoals +
                '}';
    }
}
