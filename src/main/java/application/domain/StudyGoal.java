package application.domain;

import org.codehaus.jackson.annotate.JsonBackReference;
import org.codehaus.jackson.annotate.JsonIgnore;
import org.codehaus.jackson.annotate.JsonManagedReference;
import org.codehaus.jackson.annotate.JsonProperty;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableResourceServer;

import javax.annotation.Nullable;
import javax.persistence.*;

@Entity
public class StudyGoal {

    @Id
    @GeneratedValue
    @Column(name="id")
    @Nullable
    private Integer id;

    @ManyToOne
    @Nullable
    @com.fasterxml.jackson.annotation.JsonBackReference
    private Course course;

    @ManyToOne
    private User user;

    @Column(name="hours_to_complete")
    private Integer hoursToComplete;

    @Column(name="is_done")
    @JsonProperty(value="isSuccess")
    private Boolean isDone;

    @Column(name="description")
    private String description;

    public StudyGoal(Integer id,
                     Course course,
                     User user,
                     Integer hoursToComplete,
                     Boolean isDone,
                     String description)
    {
        this.id = id;
        this.course = course;
        this.user = user;
        this.hoursToComplete = hoursToComplete;
        this.isDone = isDone;
        this.description = description;
    }

    public StudyGoal(Course course,
                     User user,
                     Integer hoursToComplete,
                     Boolean isDone,
                     String description)
    {
        this.course = course;
        this.user = user;
        this.hoursToComplete = hoursToComplete;
        this.isDone = isDone;
        this.description = description;
    }

    public StudyGoal(){}

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Course getCourse() {
        return course;
    }

    public void setCourse(Course course) {
        this.course = course;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getHoursToComplete() {
        return hoursToComplete;
    }

    public void setHoursToComplete(Integer hoursToComplete) {
        this.hoursToComplete = hoursToComplete;
    }

    public Boolean getIsDone() {
        return isDone;
    }

    public void isIsDone(Boolean done) {
        isDone = done;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    @Override
    public String toString() {
        return "StudyGoal{" +
                "\nid=" + id +
                ",\n course=" + course +
                ",\n user=" + user +
                ",\n hoursToComplete=" + hoursToComplete +
                ",\n isDone=" + isDone +
                ",\n description='" + description + '\'' +
                '}';
    }
}
